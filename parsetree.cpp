/**
 * Parse tree class
 * 26 March 2014
 * Craig Feldman
 */
 
 #include "parsetree.h"
 
 namespace fldcra001 {
	 
//=====================================================================================
	 //destructor
	 parsetree::~parsetree() {
		 delete root;
	 }
	 
	 
	 //copy constructor - calls copy const. of node on root node
	 parsetree::parsetree(const parsetree & rhs) : root(new node(*rhs.root)){
		//std::cout << "PARSETREE COPY" <<std::endl;
	 }
	
	 //copy assignment
	parsetree & parsetree::operator=(const parsetree & rhs){
		if (this != &rhs) {
			//std::cout << "PARSETREE COPY ASSIGN" <<std::endl;
			*root = *rhs.root; // call copy assign on node
		}
		
		return * this;
	}
		
		
	//move constructor
	parsetree::parsetree(parsetree && rhs) : root(std::move(rhs.root)){
		//std::cout << "PARSETREE MOVE CONST." <<std::endl;
		std::cout << root->getData();

	}
		
	//move assignment
	parsetree & parsetree::operator=(parsetree && rhs){
		//std::cout << "PARSETREE MOVE ASSIGN." <<std::endl;
		if (this != &rhs) {
			//		std::cout << "PARSETREE MOVE ASSIGN." <<std::endl;
			//if (root == nullptr)
				//root = new node(rhs.getMaxNumberChildren);
			if (rhs.root != nullptr)
				root = new node(*rhs.root);
			else root = nullptr;
		}
		
		return *this;

	}
	
//=====================================================================================

	 
	 

	 //begin
	 /** returns iterator pointing to root node */
	 fldcra001::iterator parsetree::begin() const{
		 iterator * it = new iterator(root);
		 //std::cout << it->get() << std::endl;
		 return *it;
	 }
	 
	 //end
	 fldcra001::iterator parsetree::end() const {
		 iterator * it = new iterator(); //creates nullptr iterator
		 return *it;
	 }

	
	 
	 
	 //insert
	 /**
	  * Inserts a node as a child to parent
	  * Sets to root node if no root exists
	  * Places new node in first avail location of parent.
	  * returns a new iterator pointing to inserted node
	  */	  
	  fldcra001::iterator parsetree::insert(fldcra001::iterator parent,  node & child){
		 //std::cout << "ITERATOR INSERT" << std::endl;
 
		 // check if root node exists
		 if (root == nullptr) {
			//std::cout << "Creating root node" << std::endl;
			root = child.getCopy();
			//std::cout << *it << std::endl;
			iterator *i = new iterator(root);
			return (*i);
		}
			
		else { //root exists
			if (!parent.hasSpace())
				throw std::length_error("Parent node has no space to insert");
				
			else { //insert child normally
				node * newNode = child.getCopy();

				parent.current->addChild(newNode);
				iterator *i = new iterator(newNode);
			//	delete &child;

			return (*i);
					//(parent.current)->getChildren()[parent.current->getCurrentNumberChildren()] = newNode; //set parent child to child

			}		
		}
	 }
	 
	 
	 //clear - calls destructor
	 void parsetree::clear(void){
		 if (root != nullptr)
		  delete root;
	 }
	 
	 //erase - recursively deletes the node the iterator is on
	 void parsetree::erase(iterator & i){
		// &(*i) = nullptr;
		delete &(*i);
	 }
	 
	 /**
	  * creates a random parse tree of depth totalDepth for testing purposes
	  * */
	 void parsetree::allocate(int currentDepth, int totalDepth, iterator it){


		if (currentDepth == 0) {// head
			//iterator *  begin = new iterator(this->begin());
			root = new node("Head node", 3);
			iterator * newIterator = new iterator(root);
			if (currentDepth < totalDepth)
				allocate(++currentDepth, totalDepth, *newIterator); //recursive call
			//iterator * head = new iterator (insert(*it1, *n));
			//head = iterator(it2);
		//	++counter;
			//std::cout << root->getData()  << std::endl;
			++counter;
			
		}
		
		else {
			
			// Obtain a seed from the system clock and seed the random engine
			std::size_t seed = std::chrono::system_clock::now().time_since_epoch().count();
			std::default_random_engine e(seed);
			std::uniform_int_distribution<int> intgen(0, 3);
			int children = intgen(e); // 0 to 3 children

			for (int childNum = 0; childNum < children; ++childNum){

				std::stringstream name;
				name << "Child d=" << currentDepth << " c=" << childNum;
				node * childNode = new node(name.str(), 3);
				++counter;
				
				iterator * newIterator = new iterator(insert(it, *childNode)); 			//insert the child into parent node
				delete childNode; //temporary object 
				if (currentDepth < totalDepth)
					allocate(++currentDepth, totalDepth, *newIterator);					//recursive call to generate children for children
			//iterator * newIterator = insert(it, 
			}
		}

		 
	 }
	 
	// equal
	bool parsetree::equals(parsetree & other){
		return (root->equals(*other.root));
	}
	
	// << 
	// trasverses tree calling to_string of each node
	std::ostream& operator<<(std::ostream& os, const parsetree &tree) {
		for (iterator i = tree.begin(); i != tree.end(); ++i) {
			if (i.getn() == nullptr)
				break;
			//if (++n > 20) break;
			//os << (*i).getData() << "\n";
			std::stringstream ss;
					//std::cout << (*i).getData();

			//ss << "\n";
			(*i).to_string(ss);
			//std::cout << "HERE2" << std::endl;
		}
		return os;
	}
	
	//size
	//returns size of tree
	std::size_t parsetree::size(void){
		int n = 0;
		for (iterator i = begin(); i != end(); ++i) {
			//iterator end = tree.end();
			++n;;
			if (i.getn() == nullptr)
				return n;
				//break;
		}
		return n;
	}
	
		
	 

 }
