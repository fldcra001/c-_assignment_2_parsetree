
/*
 * node.cpp
 * 
 * Class to represent a node
 * 
 * Created on 15 March 2014
 * Craig Feldman
 */

#include "node.h"

namespace fldcra001 {
	
	//destructor
	/** destroys this node and its children
	 * frees memory recursively from this node up to leaves */
	node::~node() {
		if (this != nullptr) {
			for (int i=0; i<maxChildren_; ++i) {
				if (children_ != nullptr && children_[i] != nullptr) { //prevents deletion of non existing objects
									//	std::cout << children_[i]<< std::endl;
									//		 		 				std::cout << "HERE"<< std::endl;
									//std::cout <<  children_[i]->maxChildren_  << std::endl;
									//std::cout << "DELETED" << children_[i]->data_ << std::endl;
					
					delete children_[i];

					children_[i] = nullptr;	 //to show that this slot is now open 
				}
			} 
			
			//std::cout << "Deleting: " << this->data_ << std::endl;
		}
	}
	
	//copy constructor
	node::node(const node& other):
		data_(other.data_), maxChildren_(other.maxChildren_), children_(new node * [other.maxChildren_]()), numChildren_(other.numChildren_), childNum_(other.childNum_) {
		//std::cout << "COPY CONSTRUCTOR" << std::endl;
		for (int i = 0; i < other.numChildren_ ; ++i)
		{ 
			if (other.children_[i] != nullptr){
				children_[i] = new node(*(other.children_[i]));	
			}
			else
				children_[i] = nullptr;		
		}			
	};
	
	
	//move constructor
	node::node(node && rhs): data_(std::move(rhs.data_)), maxChildren_(rhs.maxChildren_), children_(new node * [rhs.maxChildren_]()), numChildren_(rhs.numChildren_), childNum_(rhs.childNum_)
	{
		//std::cout << "MOVE CONSTRUCTOR" << std::endl;
			for (int i = 0; i < rhs.maxChildren_ ; ++i)
			{ 
				if (rhs.children_[i] != nullptr){
					children_[i] = new node(*(std::move(rhs.children_[i])));											
							//children_[i] = newNode;
							//rhs.children_[i] = nullptr;
							//*children_[i] = n;;	
				}
				else
					children_[i] = nullptr;		
				}		
				//rhs.children_ = nullptr; //wont try destroy
	};
				
		
	//copy assignment
	node & node::operator=(const node & other)
	{	
		//std::cout << "COPY ASSIGNMENT" << std::endl;
		if (this != &other)
		{
			data_ = (other.data_);
			maxChildren_ = other.maxChildren_;
			
			//copy children
			for (int i = 0; i < other.maxChildren_ ; ++i)
				{ 
					if (other.children_[i] != nullptr){
						*children_[i] = (*(other.children_[i])); //copy assign children
						//*children_[i] = node(*(other.children_[i]));	
					}
					else //set child to nullptr
						children_[i] = nullptr;
				}		
				
			numChildren_ = other.numChildren_;
			childNum_ = other.childNum_;
		}
		
		return *this;
	}
	
		
	//move assignment operator
	node & node::operator=(node && rhs){
		//std::cout << "MOVE ASSIGNMENT" << std::endl;
		if (this != &rhs) {
		
			data_ = (std::move(rhs.data_));
			rhs.data_ = "null";
			maxChildren_ = rhs.maxChildren_;
			numChildren_ = rhs.numChildren_;
			childNum_ = rhs.childNum_;
			children_ = new node * [maxChildren_]; //because constructor was not called
				for (int i = 0; i < rhs.maxChildren_ ; ++i){
					if (rhs.children_[i] != nullptr){	
						*children_[i] = (std::move(*rhs.children_[i]));

						delete rhs.children_[i];//release current resources
						//rhs.children_[i] = nullptr;
					}
					else {
						children_[i] = nullptr;
					}
	
				}
	
			rhs.children_ = nullptr; //wont try destroy
		}
		
		return *this;
	}

	//adds a child node to current node
	void node::addChild(node * n) {
		//if has space
		if (numChildren_ < maxChildren_) {
			//find empty location to add pointer to new child in parent 
			for (int i=0; i<=maxChildren_; ++i) {
				if (children_[i] == nullptr) {
					//std::cout << "INSERTing " << n->data_ << "into position " << i << std::endl;
					//n->parent_ = *this;
					
					n->childNum_ = i; //place in children array
					children_[i] = n;	
					
					break;				
				}
			}
			++numChildren_; //increase index to mark next free space for child
		}		
		else std::cerr << "Error: no room to insert " <<  std::endl;
		
	}
	
	/**creates a string stream containing the nodes value
	 * only adds data to ss if it is a leaf*/
	 void node::to_string(std::stringstream& ss){
		 				//std::cout << "CALLED Base to string" << std::endl;

		if (numChildren_ == 0) //has no children therefore leaf
			std::cout << ss.str() << data_ ;
	}

	
	//returns if a node and its children equals another node
	bool node::equals(node & other) {
		if (numChildren_ != other.numChildren_ || maxChildren_ != other.maxChildren_ || data_ != other.data_) 
			return false;
		else {
			//check children are equal
			if (hasChildren() && other.hasChildren()) {
				for (int i=0; i<maxChildren_; ++i) {
					if (children_[i] == nullptr && other.children_[i] == nullptr) // null node is equal
						return true;
					
					if (children_[i] == nullptr && other.children_[i] != nullptr)
						return false;
						
					else if (other.children_[i] == nullptr && children_[i] != nullptr)
						return false;
						
					else if (!(children_[i]->equals(*other.children_[i])))
						return false;
					
				}
			}
			
		}
		
		return true;
	}
	
	
// 									DERIVED CLASSES	
// =====================================================================================================
	// compound node
	
	void compoundNode::to_string(std::stringstream& ss)  {
		//std::cout<<"COMPOUND TOSTRING CALLED"<<std::endl;
		if (hasChildren()) {
					//std::cout << ss.str() << "Compoundss" << std::endl;
		}
		else
			node::to_string(ss);
	}
	
	node * compoundNode::getCopy() {
		return (new compoundNode(*this));
	}
	
// =====================================================================================================
	// statement node
	
	void statementNode::to_string(std::stringstream& ss)  {
		if (hasChildren()) {
					//std::cout << ss.str() << "statementss" << std::endl;
		}
		else
			node::to_string(ss);
	}
	
	node * statementNode::getCopy() {
		return (new statementNode(*this));
	}

// =====================================================================================================
	// expression node
	
	void expressionNode::to_string(std::stringstream& ss)  {
		/**
			if (getChildNum() == 1){  //middle child add spaces on eithe side
				const std::string &temp = ss.str();
				ss.seekp(0);
				ss << " ";
				ss << temp << " ";
			}
			* */
				
		//	node::to_string(ss);
	}
	
	node * expressionNode::getCopy() {
		return (new expressionNode(*this));
	}

// =====================================================================================================
	// if node
	
	void ifNode::to_string(std::stringstream& ss)  {
		if (hasChildren()) 
					std::cout << "\n" << "\t" << "if " ;
		else
			node::to_string(ss);
	}
	
	node * ifNode::getCopy() {
		return (new ifNode(*this));
	}

// =====================================================================================================

	// while node
	
	void whileNode::to_string(std::stringstream& ss)  {
						//	std::cout << "WHILECALLED" << std::endl;

		ss << "\n" << "while ";
		std::cout << ss.str() ;
	}
	
	node * whileNode::getCopy() {
		return (new whileNode(*this));
	}

// =====================================================================================================		
	// function node
	
	void functionNode::to_string(std::stringstream& ss)  {
		std::cout << ss.str() << "\n\t  ";
	}
	
	node * functionNode::getCopy() {
		return (new functionNode(*this));
	}

// =====================================================================================================
	 

} //namespace
