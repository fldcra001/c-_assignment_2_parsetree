# make file
# CSC 3022H Assignment 2
# Craig Feldman

CC = g++				# compiler	
CXXFLAGS=-std=c++0x		# flags for the c++ compiler; header +all warnings; c++11
TARGET=tree						# executable name
OBJECTS= node.o	driver.o iterator.o parsetree.o			# object files to build into exe

# Linking Rule
$(TARGET): $(OBJECTS) $(HEADERS)
	$(CC) $(OBJECTS) -o $(TARGET)
#@cp $(TARGET) ./binaries 				#move to binaries

driver.o: driver.cpp
node.o: node.cpp node.h
iterator.o: iterator.cpp iterator.h
parsetree.o: parsetree.cpp parsetree.h



# Macro to associate *.o with *.cpp
.cpp.o:
	$(CC) $(CXXFLAGS) -c $<

clean:
	@rm -f *.o
