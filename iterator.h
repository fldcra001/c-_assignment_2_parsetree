/*
 * iterator.h
 * 
 * Created on 20 March 2014
 * Craig Feldman
 */

#ifndef ITERATOR_H
#define ITERATOR_H

#include "node.h"
#include <stack>
#include <utility> //pair

namespace fldcra001 {
	class iterator {
		friend class parsetree;
	private:
		node * current;
		mutable std::stack<std::pair<node*, int> > stack;
		int childNum = 0;
		bool end = false; //boolean to track if iterator has reached end
		int currentNum = 0;
		std::pair<node *, int> p = std::make_pair(nullptr, 0); //default pair to allow iterator to commence
	
		//constructor
		iterator(node * n) : current(n) {};


	public:
			//node * current;

						iterator() :current(nullptr) {};

		
		//destructor
		~iterator() {};
		
		
		
		// copy constructor
		iterator (const iterator & other);
		
		//move constructor 
		iterator (iterator && other);

		
		//copy assignment
		iterator & operator=(const iterator & other) {};//std::cout<<"copy assignment iterator called"<<std::endl;};
		
		// *
		node& operator*(void);

		// ==
		bool operator==(const iterator & rhs);
		// !=
		bool operator!=(const iterator & rhs);
		
		// ++
		iterator& operator++(void);
		// +
		iterator& operator+(int n);
		
		// --
		iterator& operator--(void);
		// - 
		iterator& operator-(int n);
		
		// distance
		int operator-(iterator prev);
		
		
		
		//returns if iterators node has children
		bool hasChildren(void)
		{	return current->hasChildren();}
		
		bool hasSpace(void)
		{	return current->hasSpace();}
		
		std::string get(void) const{return current->getData();}
		
		int getChildNum(void) const 
		{return current->getChildNum();} 
		
		node * getn() {return current;};
			
		int getMaxNumberChildren(void) const
		{return current->getMaxNumberChildren();} 
		
		void copy_reverse(std::stack<std::pair<node*, int> > source, std::stack<std::pair<node*, int> > dest);
		
	};// class
} //namespace

#endif
