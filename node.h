/*
 * node.h
 * 
 * Created on 15 March 2014
 * Craig Feldman
 */

#ifndef NODE_H
#define NODE_H

#define TESTING_PTREE //for debug

#include "counter.h"
#include <sstream>

namespace fldcra001 {
	class node
	#ifdef TESTING_PTREE
	: public sjp::counter<node>
	#endif
	{
		
		private:
			std::string data_ = ""; 		//what is stored in the node
			int maxChildren_; 				//maximum children number
			node** children_; 				//points to array of pointers that point to children

			int numChildren_ = 0;			//current number of children a node has

			int childNum_ = 0;;

		
		public:

			
			
			//constructor
			node(std::string data, int maxChildren) : data_(data), maxChildren_(maxChildren), children_(new node * [maxChildren]()) {
			};
			
			node(){};
						
			//destructor
			virtual ~node();	
					
				
			//copy constructor
			node(const node& other);
				
			//copy assignment
			node & operator=(const node & other);
			
			
			//move constructor
			node(node && rhs);
				
							
			//move assignment operator
			node & operator=(node && rhs);

			
			
			/*
			int getCurrentChildren (void) {
				return n_;
			}
			
			* */
			
				
			void addChild(node * n);
			
			virtual void to_string(std::stringstream& ss);
			
			bool equals(node & other);
			
			
			int getMaxNumberChildren(void) const
			{return maxChildren_;}
			
			int getCurrentNumberChildren(void) const
			{return numChildren_;}
			
			bool hasChildren(void) const
			{return (numChildren_ > 0);}
			
			bool hasSpace(void) const
			{return (maxChildren_ != numChildren_);}
			
			node ** getChildren(void)
			{return children_;}
			
			//returns a child pointer to specified child num
			node * getChild(int i)
			{return children_[i];}
			
			int getChildNum(void)
			{return childNum_;}
			
			std::string& getData(void)
			{return data_;}
			
			//allows calls to copy constructor from upcast derived object
			virtual node * getCopy(){
				return new node(*this); 
			}
		
		
	}; //class
	
	class compoundNode : public node {
		public:
	
		compoundNode(int max) : node("", max) {};	
		compoundNode(const compoundNode & n) : node(n){};
		
		virtual void to_string(std::stringstream& ss) override ;
		
		virtual node* getCopy();
		
	};
	
	class statementNode : public node {
		public:
		statementNode() : node("statement", 3){};	
		statementNode(const statementNode & n) : node(n){};
		
		virtual void to_string(std::stringstream& ss) override;	
		virtual node* getCopy();
	};
	
	class expressionNode : public node {
		public:
		expressionNode(int max) : node("expression", max){};	
		expressionNode(const expressionNode & n) : node(n){};

		
		virtual void to_string(std::stringstream& ss) override;
		virtual node* getCopy();
	
	};
	
	class ifNode : public node {
		public:
		ifNode() : node("if", 4){};	
		
		virtual void to_string(std::stringstream& ss) override;
		virtual node* getCopy();
	};
	
	class whileNode : public node {
		public:
		whileNode() : node("while", 3){};	
		whileNode(const whileNode & n) : node(n){};
		
		virtual void to_string(std::stringstream& ss) override;
		virtual node* getCopy();	
	};
	
	class functionNode : public node {
		public:
		functionNode(int max) : node("function", max){};	
		
		virtual void to_string(std::stringstream& ss) override;
		virtual node* getCopy();	
	};
}//namespace

#endif //NODE_H
