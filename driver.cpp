/**
 * driver for CSC3022H Assignment 2
 * Parse Tree
 * Craig Feldman
 * 15 March 2014
 */
 
 #include <random>				// For random number generation
 #include <chrono>				// For accessing the system clock
 #include <sstream>

 #define CATCH_CONFIG_MAIN 		// So that Catch will define a main method
 #include "catch.hpp"    		// Catch unit testing framework
 
 #include "node.h"
 #include "iterator.h"
 #include "parsetree.h"
 
 using fldcra001::node;
 using fldcra001::parsetree;
 using fldcra001::iterator;
 using std::cout;
 using std::endl;
 
 TEST_CASE("Node"){
	 
	// Obtain a seed from the system clock and seed the random engine
    // Create random ints between 0 10
    std::size_t seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine e(seed);
    std::uniform_int_distribution<int> intgen(1, 10);

    // Get a random integer between 0 and 10
    const int N = intgen(e);
	
	node * testNode;
	//node * testNodeChild;
	node * testNodeCopy;
	node * testNodeMove;
	
	 
	 // constructor
	 SECTION("Testing node constructor") {
		 testNode = new node("Test Node", N);
		 //INFO("Constructing a node with space for 3 children");
		 REQUIRE (node::objects_alive == 1);
		 REQUIRE (testNode->getMaxNumberChildren() == N);
	 } //constructor test
	 
	 // adding child
	 SECTION("Testing that a child can be added to constructed node") {
		 node *testNodeChild = new node("Child", 0);
		 testNode->addChild(testNodeChild);
		 REQUIRE (node::objects_alive == 2);
		 REQUIRE (testNode->hasChildren() == true);
	 }
	 
	 // copy constructor
	 SECTION("Testing copy constructor on head node with a child") {
		 testNodeCopy = new node(*testNode);
		 REQUIRE (node::objects_alive == 4);
		 REQUIRE (testNode->equals(*testNodeCopy) == true);
	 }
	 
	
	 
	  // copy assignment
	 SECTION("Testing copy assignment on head node with a child") {
		 *testNodeCopy = *testNode;
		 REQUIRE (node::objects_alive == 4);
		 REQUIRE (testNode->equals(*testNodeCopy) == true);
		
	 }
	
	
	  // move constructor	
	 SECTION("Testing move constructor on head node with a child") {
		 node testNodeMove = (std::move(*testNode)); 
		 INFO ("+2 due to move constructor constructing 2 additional objects");
		 REQUIRE (node::objects_alive == 4 + 2);
	 }
	 
	
	
		
	 // move assignment
	 SECTION("Testing move assignment on head node with a child (Also tests previous moved node is in useable state)") {
		 testNodeMove = new node();
		 *testNodeMove = std::move(node("Move", 2));
		 INFO ("+1 due to having to create a new node to move a node into");
		 REQUIRE (node::objects_alive == 4 + 1); //should have no new obj created and previous move objects deleted
	 }
	 
	  // destructor
	 SECTION("Testing destructor") {
		INFO ("Deleting all dynamically allocated nodes");
		 delete testNode;
		 delete testNodeCopy;
		 delete testNodeMove;
		 REQUIRE (node::objects_alive == 0 ); //should have no alive objects
	 }
	 
 
}



//========================================================================================================

TEST_CASE("ParseTree"){
	
	parsetree * tree;
	parsetree * treeCopy;
	parsetree * treeCopy2;
	parsetree * treeMove;
	//parsetree * treeMove;

	SECTION("Random parsetree construction") {
		
		REQUIRE (node::objects_alive == 0 ); //should have no alive objects at this point
		
		// Obtain a seed from the system clock and seed the random engine
		// Create random ints between 0 10
		std::size_t seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::default_random_engine e(seed);		
		std::uniform_int_distribution<int> intgen(3, 10);
		// Get a random integer between 3 and 10 for tree depth
		const int DEPTH = intgen(e)	;

		//create random size parsetree
		tree = new parsetree();
		//cout << "DEPTH " << DEPTH << endl;
		iterator it;
		tree->allocate(0, DEPTH, it); //calls a function to allocate a random tree of depth DEPTH starting from depth 0

		INFO ("Created a random parsetree");
		REQUIRE (node::objects_alive == tree->getCounter()); 
	} // end construction test
	
	 SECTION("Parsetree size() method") {
		 INFO ("Checking the size of tree computed by size() method is correct");
		 REQUIRE (tree->size() == tree->getCounter());
	 }
	
	 SECTION("Parsetree copy constructor") {
		treeCopy = new parsetree(*tree);
		INFO ("Checking there are twice as many objects alive");
		REQUIRE (node::objects_alive ==  (tree->size()) * 2);
		INFO ("Checking the two trees are equal");
		REQUIRE (treeCopy->equals(*tree));
	 }
	
	 // copy assignment
	 SECTION("Parsetree copy assignment") {
		//parsetree * treeCopyAssign = new parsetree();
		*treeCopy = *tree;
		INFO ("Checking that objects have been assigned and not copy constructed");
		REQUIRE (node::objects_alive ==  (tree->size()) * 2);
		INFO ("Checking the two trees are equal");
		REQUIRE (treeCopy->equals(*tree));
	 }
	 
	 //move constructor
	 SECTION("Parsetree move constructor") {
		 parsetree treeMove = std::move(*treeCopy); 
		 INFO ("Checking no extra objects have been added");
		 REQUIRE (node::objects_alive == (tree->size()) * 2);
		 INFO ("Checking the trees are equal (note had copy of moved tree [tree] to test against)");
		 REQUIRE (treeMove.equals(*tree));
	 }
	 
	 // move assignment
	 SECTION("Parsetree move assignment") {
		 treeCopy2 = new parsetree(*tree); 	//for checking against later
		 treeMove = new parsetree();
		 *treeMove = std::move(parsetree(*tree));
		 INFO ("Checking no new objects created. Note *3 due to having 2 extra copies existing now and the moved tree.");
		 REQUIRE (node::objects_alive == (tree->size()) * 3);
		 INFO ("Note 'treeCopy2' is a copy of moved tree to test equality against");
		 REQUIRE (treeMove->equals(*treeCopy2));
		 delete treeMove;
	 }
	 

	 
	SECTION("Parsetree destruction") {
		tree->clear();
		treeCopy2->clear();
		INFO ("Called clear() method on all dynamically allocated trees");
		REQUIRE (node::objects_alive == 0);
	}
	
		 // erase - erases subtree iterator points to
	 SECTION ("Parsetree erase") {
		

		std::size_t seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::default_random_engine e(seed);		
		std::uniform_int_distribution<int> intgen(3, 10);
		// Get a random integer between 3 and 10 for tree depth
		const int DEPTH = intgen(e)	;

		//create random size parsetree
		parsetree * tree = new parsetree();

		iterator it;
		tree->allocate(0, DEPTH, it); //calls a function to allocate a random tree of depth DEPTH starting from depth 0

		INFO ("Random size tree created");
		 REQUIRE (node::objects_alive > 0 ); //Created a tree

		iterator i = iterator(tree->begin());
		tree->erase(i);

		 //tree->erase(i2);
		 INFO ("Created a new random tree. Called erase() on root subtree should leave 0 nodes alive.")
		 REQUIRE (node::objects_alive == 0);

		 
	 }

} //parsetree test

//=======================================================================================================


TEST_CASE("Iterator"){
	
	parsetree * tree;
	
	SECTION ("Iterate from begin to end (tests equalities and inequalities and +iterator)") {
		REQUIRE (node::objects_alive == 0 ); //should have no alive objects at this point
		
		// Obtain a seed from the system clock and seed the random engine
		// Create random ints between 0 10
		std::size_t seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::default_random_engine e(seed);		
		std::uniform_int_distribution<int> intgen(3, 10);
		// Get a random integer between 3 and 10 for tree depth
		const int DEPTH = intgen(e)	;

		tree = new parsetree();
		//cout << "DEPTH " << DEPTH << endl;
		iterator it;
		tree->allocate(0, DEPTH, it); //calls a function to allocate a random tree of depth DEPTH starting from depth 0
		
		cout << *tree;

		//INFO ("Note: ");

		REQUIRE (node::objects_alive == tree->size());
	} 
	
	SECTION ("Find distance between two iterators") {
		iterator it1 =  iterator(tree->begin());
		iterator it2 = iterator(tree->begin());
		INFO ("Distance between 2 equal iterators is defined as 0");
		REQUIRE ( (it2 - it1) == 0);		
	}
	
	SECTION ("Iterator +, -") {
		parsetree tree1 = parsetree();
		parsetree tree2 = parsetree();
		node * head = new node ("head", 4);
		node * n1 = new node ("n1", 4);
		node * n2 = new node ("n2", 4);
		node * n3 = new node ("n3", 4);
		head->addChild(n1);
		head->addChild(n2);
		head->addChild(n3);
		iterator start;
		iterator it1 = tree1.insert(start, *head);
		it1 + 2;
		
		iterator it2 = tree2.insert(start, *head);
		++it2; ++it2;
		//it2 + 2;
		INFO ("Check that moving forward by calling +2 is equivalent to two calls to ++ (calls done in code)");
		REQUIRE (it1==it2);		
		
		INFO ("Check that moving back by calling -2 is equivalent to two calls to -- (calls done in code)");
		REQUIRE (it1==it2);	
	}
}

//=======================================================================================================



TEST_CASE("Example") {
	SECTION ("Creating a demo tree (figure 1 of assignment)") {
		INFO ("Outputing figure 1 tree");
		REQUIRE (true);
		parsetree tree = parsetree();
		iterator start;
			
		node * head = new node("assignment statement", 4);
		node * n1 = new node("identifier", 1); 
		//head = tree.insert(head, *n1);

		//actually did this near the beginning hence why using add and not insert.
		node * n2 = new node("x3", 0);
		n1->addChild(n2);
		head->addChild(n1);
	 
		node * n3 = new node("=", 0);
		head->addChild(n3);

		node * n4 = new node("expression", 3);
		head->addChild(n4);
		node * n5 = new node("expression", 1);
		n4->addChild(n5);
		node * n6 = new node("identifier", 1);
		n5->addChild(n6);
		node * n7 = new node("y", 0);
		n6->addChild(n7);	
	
		node* n8 = new node("+", 0 );
		n4->addChild(n8);
		
		node * n9 = new node("expression", 1);
		n4->addChild(n9);
		node * n10 = new node("number", 1);
		n9->addChild(n10);
		node * n11 = new node("3", 0);
		n10->addChild(n11);
		
		node * n12 = new node(";", 0);

		head->addChild(n12);
				start = tree.insert(start, *head);
				
				
		std::cout << tree << endl;;
	}
}

//=======================================================================================================


TEST_CASE("Polymorphism") {
	using namespace fldcra001;
	SECTION ("Example from assignment") {
		INFO ("Demonstrates polymorphic calls to 'to_string()'");
		REQUIRE(true);
		
		parsetree tree = parsetree();
		std::stringstream ss;
		iterator start;
		iterator it;

		//very messy stuff here :{
		
		node * c = new compoundNode(4);
		iterator head = tree.insert(start, *c);

			tree.insert(head, *(new node("{\n" , 0)));
			
			node * s1 = new statementNode();
				s1->addChild(new node("a", 0));
				s1->addChild(new node(" = ", 0));
				node * e1 = new expressionNode(1);
			iterator its1 = tree.insert(head, *s1);
				iterator ite1 = tree.insert(its1, *e1);
					tree.insert(ite1, *(new node("0", 0)));

			
			node *w1 = new whileNode();
				iterator itw1 = tree.insert(head, *w1);

				node * e2 = new expressionNode(3);
				e2->addChild(new node("(", 0));
				iterator ite2 = tree.insert(itw1, *e2);
				node * e3 = new expressionNode(3);
					e3->addChild(new node("a", 0));
					e3->addChild(new node(" < ", 0));
					e3->addChild(new node("10", 0));
					tree.insert(ite2, *e3);
				tree.insert(ite2, *(new node(")", 0)));
				
				
				
			node *c2 = new compoundNode(4);
				iterator itc2 = tree.insert(itw1, *c2);
				tree.insert(itc2, *(new node("{", 0)));
				
				node* ifN = new ifNode();
					iterator itIf = tree.insert(itc2, *ifN);
				
					// 1st child of if
					node * e4 = new expressionNode(3);
						e4->addChild(new node("(", 0));
						iterator ite4 = tree.insert(itIf, *e4);
						node * e5 = new expressionNode(3);
							iterator ite5 = tree.insert(ite4, *e5);
							node* e6 = new expressionNode(3);
								e6->addChild(new node("a", 0));
								e6->addChild(new node(" % ", 0));
								e6->addChild(new node("2", 0));
								iterator ite6 = tree.insert(ite5, *e6);
							tree.insert(ite5, *(new node(" == ", 0)));
							tree.insert(ite5, *(new node(" 0", 0)));
						tree.insert(ite4, (*(new node(")", 0))));
						
					// 2nd child of if
					node * c3 = new compoundNode(3);
						c3->addChild(new node("{" , 0));
						iterator itc3 = tree.insert(itIf, *c3);
						node * f1 = new functionNode(2);
							f1->addChild(new node("print ", 0));
							f1->addChild(new node("'even'", 0));
						tree.insert(itc3, *f1);
						tree.insert(itc3, (*(new node("\n\t}", 0))));
						
					//3rd child of if
					tree.insert(itIf, (*(new node("\n\telse", 0))));
					
					// 4th child of if
					node * c4 = new compoundNode(3);
						c4->addChild(new node("{" , 0));
						iterator itc4 = tree.insert(itIf, *c4);
						node * f2 = new functionNode(2);
							f2->addChild(new node("print ", 0));
							f2->addChild(new node("'odd'", 0));
						tree.insert(itc4, *f2);
						tree.insert(itc4, (*(new node("\n\t}\n", 0))));
						
				//last statement node			
				node * s2 = new statementNode();
					s2->addChild(new node("\ta", 0));
					s2->addChild(new node(" += ", 0));
					node * e7 = new expressionNode(1);
					iterator its2 = tree.insert(itc2, *s2);
					iterator ite7 = tree.insert(its2, *e7);
					tree.insert(ite7, *(new node("1", 0)));
	
					

							
				
			//insert compound under while
			tree.insert(itw1, *c2);
			
			//last child of head
			it = tree.insert(head, *(new node("\n}" , 0)));


		//output the tree
		std::cout << tree << std::endl;

	
	}
}
//=======================================================================================================
//=======================================================================================================




   // iterator *  head = tree->begin();
    
    /*
    for (int d = 0; d < DEPTH; ++d)
    {
		std::uniform_int_distribution<int> intgen(0, 2);
		children = intgen(e); // 0 to 3 children
		    cout << "CHILDREN " << children << endl;

		if (d == 0) {// head
			iterator *  begin = new iterator(tree->begin());
			node * n = new node("Head node", 3);
			head = new iterator (tree->insert(*begin, *n));
			//head = iterator(it2);
			++counter;
			cout << head->get()  << endl;
		}
		else {
			for (int childNum = 0; childNum < children; ++childNum){

				std::stringstream name;
				name << "Child d=" << d << " c=" << childNum;
				node * childNode = new node(name.str(), 3);
				if (d == 0)
					it1 = (tree->insert(*head, *childNode));
				else { 
					if (childNum = 0)
						it1 = (tree->insert(it, *childNode));
					if (childNum = 1)
						it2 = (tree->insert(it2, *childNode));
						
					
				counter++;
						//	cout << *it  << endl;
			}
		}
	}
*/

		
		/*
	
		
	parsetree *  tree = new parsetree(); 
	iterator *  head = new iterator(tree->begin());
	
	node* p = new node("testing", 3);
	node* p1 = new node("child 1", 2);
	node* p2 = new node("child 2", 2);


	iterator  it = (tree->insert(*head, *p));
	iterator it2 =  tree->insert(it, *p1);
	iterator it3 =  tree->insert(it, *p2);

	cout << *it << " HEAD" << endl;
	++it;
	cout << *it << " C1" << endl;

	++it;
		cout << *it << " HEAD" << endl;

	++it;
		cout << *it << " C2" << endl;
		
		++it;
		cout << *it << " C2" << endl;
		++it;
		cout << *it << " C2" << endl;
				++it;
		cout << *it << " C2" << endl;

	//cout << *it2 << endl;
	//cout << *it3 << endl;
	
	
*/

			
			//	node* p1 = new node("child", 2);


				









 
 /*
 
 int main() {
	 using namespace fldcra001;
	 using std::cout;
	 using std::endl;
	 
	 {
	 cout << "BEGIN TESTING NODE CLASS\n" << endl;
	 
	 //test constructor
	 cout << "Testing constructor:" << endl;	 
	 node * n1 = new node("H", 2);
	 node * n2 = new node("C1", 0);
	 node * n3 = new node("C2", 0);
	 n1->addChild(n2);
	 n1->addChild(n3);
	 cout << "Created a head node with two children." << endl;	 
	 node::print_counts(std::cout,"node"); //node count

	 //test copy constructor
	 cout << "\nTesting copy constructor:" << endl;	
	 cout << "Copying head node" << endl;
	 node * n1copy;
	 n1copy = new node(*n1);	
	 cout << "Original head node contents:" << endl;
	 cout << "Head: " << n1->getData() << "; Child 1: " << (n1->getChildren()[0])->getData() << "; Child 2: " << (n1->getChildren()[1])->getData()  << endl;
	 cout << "Copied head node contents:" << endl;
	 cout << "Head: " << n1copy->getData() << "; Child 1: " << (n1copy->getChildren()[0])->getData() << "; Child 2: " << (n1copy->getChildren()[1])->getData()  << endl;
	 node::print_counts(std::cout,"node");

	 

	 //test copy assignment
	 cout << "\nTesting copy assignment:" << endl;	
	 cout << "Copying head node" << endl;
	 *n1copy = (*n1); 
	 cout << "Copied head node contents:" << endl;
	  cout << "Head: " << n1copy->getData() << "; Child 1: " << (n1copy->getChildren()[0])->getData() << "; Child 2: " << (n1copy->getChildren()[1])->getData()  << endl;
	 node::print_counts(std::cout,"node"); 
	// cout << "Head: " << n1->getData();
	 
	
	 //test move constructor
	 cout << "\nTesting move constructor:" << endl;	
	 cout << "Moving head node" << endl;
	 node::print_counts(std::cout,"node"); 
	 node n1Move = (std::move(*n1copy));
	 cout << "Head: " << n1Move.getData() << "; Child 1: " << (n1Move.getChildren()[0])->getData() << "; Child 2: " << (n1Move.getChildren()[1])->getData()  << endl;
	 cout << "Head: " << n1copy->getData() << endl;
	 node::print_counts(std::cout,"node"); 
	 
	 //test move assignment
	 cout << "\nTesting move assignment:" << endl;	
	 cout << "Moving a head node back to original node" << endl;
	 node::print_counts(std::cout,"node"); 	 
	 //node * n1Move2;
	 *n1 = (std::move(n1Move));
	  cout << "Contents of destination node: " << endl;
	 cout << "Head: " << n1->getData() << "; Child 1: " << (n1->getChildren()[0])->getData() << "; Child 2: " << (n1->getChildren()[1])->getData()  << endl;
	 cout << "Contents of source node: " << n1Move.getData() << endl;
	 
	 //test delete
	 cout << "\nTesting node delete on alive nodes" << endl;	
	 node::print_counts(std::cout,"node"); 
	 delete n1copy;
	 delete n1;
	 
	} //scope for automatic deletion of certain objects
	 cout << "Deleted" << endl;
 	 node::print_counts(std::cout,"node"); 
 	 	 
 	 cout << "END NODE TEST\n" << endl;




	 
	 
	 


	 
	
	 
	 


	 

	  

	 
	 
	 
	 /*
	 //node head (node("head", 3));
	 node * head = new node("assignment statement", 4);
	 
	 node * n1 = new node("identifier", 1); 
	 head->addChild(n1);
	 node * n2 = new node("x3", 0);
	 n1->addChild(n2);
	 
	node * n3 = new node("=", 0);
	head->addChild(n3);

	node * n4 = new node("expression", 3);
	head->addChild(n4);
	node * n5 = new node("expression", 1);
	n4->addChild(n5);
	node * n6 = new node("identifier", 1);
	n5->addChild(n6);
	node * n7 = new node("y", 0);
	n6->addChild(n7);	
	
		node* n8 = new node("+", 0 );
		n4->addChild(n8);
		
		node * n9 = new node("expression", 1);
	n4->addChild(n9);
	node * n10 = new node("number", 1);
	n9->addChild(n10);
		node * n11 = new node("3", 0);
		n10->addChild(n11);
		
			node * n12 = new node(";", 0);

		head->addChild(n12);

	
	
	iterator it(head);	 
	iterator it2(head);	 

	cout << (*it) << endl;
	++it;
	++it;
	++it;
	++it;
	++it;
		++it;
	++it;
	++it;
	++it;
	++it;
	++it;
	++it;
	*/
	
	
	//delete head;
	//delete &copy; 		 
	
	/*
	parsetree *  tree = new parsetree(); 
		 
	node* p = new node("testing", 0);
	node* p1 = new node("child", 2);
	iterator *i = new iterator(p);	

	iterator it2 = tree->insert(*i, *p);
	
	std::cout << "2" << std::endl;
	if (it2 == nullptr)
	cout << "nullptr" << endl;
	else
		cout << "not nullptr" << endl;
	cout << *it2 << endl;



	 	 	 
	 cout << "\nFinal count:" << endl;
	 node::print_counts(std::cout,"node"); //final count
	 
	 return 0;
 } //end main
 */
 
