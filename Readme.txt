**The original assignment requirements and instructions can be found in the 'Downloads' section.**

*********************
Author: Craig Feldman
Date: 	02 April 2014
*********************

=======================
Assignment 2 - Parsetree
Readme.txt
========================


----------------
  Instructions
----------------

 1. Compile the application using the 'make' command.

 2. Due to the large number of tests, you should run each section individually as follows

	./tree -s Node 
	./tree -s ParseTree
	./tree -s Iterator
	./tree -s Example
	./tree -s Polymorphism


	NOTE these tests have been performed:
		Node:
			6 Special member functions
			Adding children

		Parsetree: 
			constructs a random size parsetree
			6 Special member functions
			size() method
			equals() method to check equality i.e.. to test a copy is correct.
			begin() and end() test
			clear()
			erase method to erase from iterator location.
			insert tested by creating random tree.
			<<

		Iterator:
			iterate from begin() to end()
			Copy constructor (implicit - used in code)
			+, -, ++, --, *
			==, != (implicitly tested)
			- (to return distance between iterators)

		Example:
			Demonstrates creating tree as shown in figure 1 of assignment and outputting it.

		Polymorphism:
			Demonstrates node specialisation and polymorphic to_string() calls.
			Demonstrates the example in tut (figure 2) code output.
			
			
 3. Check that all test cases and assertions succeed.

 4. Run 'make clean' if you want to remove generated files.


--------------------------
  List of provided files
--------------------------

	catch.hpp
	counter.hpp
	
	iterator.cpp	iterator.h			- Performs iteration on tree
	parsetree.cpp	parsetree.h			- Container for tree
	node.cpp		node.h				- Node class which also contains derived classes	
	driver.cpp							- driver that runs the tests

	makefile

	Readme.txt

	Git repo


--------
  Info
--------

I used Ubuntu on a virtual machine for this project. All code has been tested and compiles and runs as expected on the senior lab pcs.
'Geany' was used as the IDE.
 
The makefile generates an executable called 'tree'.

Please see the files for further details.

All the required tests have been performed and the tests are explained in the program.

Note that I included the class definitions for the node subclasses in original node file as they were minor implementations and logically should be grouped.

		  
-------------------
  Troubleshooting
-------------------

 Ensure the all files exist in the same directory.
 This program uses c++ 11 features. So please ensure you have the latest version/compiler.

 

 Enjoy!