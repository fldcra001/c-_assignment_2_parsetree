/**
 * Iterator for parse tree
 * 20 March 2014
 * Craig Feldman
 */
 
 #include "iterator.h"
 
 namespace fldcra001 {
	 
	//move constructor
	iterator::iterator(iterator && other) : childNum(other.childNum), end(other.end), currentNum(other.currentNum) {
				//std::cout << "Move CONSTRUCTOR ITERATOR" << std::endl;
				current = other.current;

	}
	

	//copy constructor
	//creates deep copy of iterator
	iterator::iterator(const iterator &other) : childNum(other.childNum), end(other.end), currentNum(other.currentNum) {
		//std::cout << "COPY CONSTRUCTOR ITERATOR" << std::endl;	
	//copy stack
		current = other.current;
	if (other.stack.size()<500){
		std::stack<std::pair<node*, int> > tmp;
		//std::cout << "HERE" << std::endl;
		//std::cout << other.stack.size() << std::endl;
		//std::cout << "HERE" << std::endl;


		while(!(other.stack).empty()) {
							//std::cout << other.stack.size() << std::endl;

			std::pair<node *, int> p = other.stack.top();
			std::pair<node *, int> p2 = std::make_pair(p.first, p.second);
			tmp.push(p2);
			other.stack.pop();
		}

		while(!tmp.empty()) {
			std::pair<node *, int> p = tmp.top();
			std::pair<node *, int> p2 = std::make_pair(p.first, p.second);
			this->stack.push(p2);
			tmp.pop();
		} 
	}
		//iterator::copy_reverse(other.stack, tmp);
		//copy_reverse(tmp, this->stack);

		

	}
	

	
	// *
	// returns ref to current node that iterator references
	node & iterator::operator*()
	{ 
		//if (current != nullptr)
			return *current; 
	} 
			
	// ==
	bool iterator::operator ==(const iterator & rhs)
	{		//return (current->equals(*rhs.current));}
		if (current == nullptr && rhs.current == nullptr)
			return true;
		else if (current == nullptr && rhs.current != nullptr)
			return false;
		else if (rhs.current == nullptr &&  current != nullptr)
			return false;
		else return (current->equals(*rhs.current));
	}

	// !=
	bool iterator::operator !=(const iterator & rhs)
	{		
		if (current == nullptr && rhs.current == nullptr)
			return false;
		else if (current == nullptr && rhs.current != nullptr)
			return true;
		else if (rhs.current == nullptr &&  current != nullptr)
			return true;
		else return (!current->equals(*rhs.current));
	}
	
	// ++
	iterator& iterator::operator++() {
		if (end == true){
			iterator *toReturn = new iterator(); //creates a nullptr wrapped by iterator
				//	std::cout << "ENDDDD" << std::endl;
			current == nullptr;
			return *toReturn;
		}

		//initialise children variable
		node ** children = nullptr;
		if (current->hasChildren())
			children = current->getChildren();		
				
		
		//if the current has children and we still have children to explore, go deeper
		if (current->hasChildren() && (currentNum < current->getMaxNumberChildren()) && (children[currentNum] != nullptr)) {
				
			//find next child 	
			for (int i = currentNum; i < current->getMaxNumberChildren(); ++i) {
				if (children[i] != nullptr) {
					
					//push the parent as well as current child num onto stack
					p = std::make_pair(current, i);
					stack.push(p);
					current = children[i];
					break;
				}
			}
			currentNum = 0;
		}
		
		else { //leaf therefore pop last state and move onto next sibling of parent.
			//check if we are on last node
			if (stack.empty()){
				end = true;
				iterator *toReturn = new iterator(); //creates a nullptr wrapped by iterator
				current = nullptr;
				return *toReturn;
			}
			
			p = stack.top();
			stack.pop();
			current = p.first;
			currentNum = p.second+1;
			operator ++();
		}

		return *this;	
	}
	
	// +
	//moves iterator forward by n steps
	iterator& iterator::operator+(int n){
		for (int i=0 ; i<n-1; ++i)
			operator++();
		return operator++();
	}
	
	// --
	// moves iterator back 1
	iterator& iterator::operator--() {
		if (end == true){
			iterator *toReturn = new iterator(); //creates a nullptr wrapped by iterator
				//	std::cout << "ENDDDD" << std::endl;
			current == nullptr;
			return *toReturn;
		}

		//initialise children variable
		node ** children = nullptr;
		if (current->hasChildren())
			children = current->getChildren();		
				
		
		if (current->hasChildren() && (currentNum < current->getMaxNumberChildren()) && (children[currentNum] != nullptr)) {
				
			//go to previous child	
			for (int i = currentNum; i > 0 ; --i) {
				if (children[i] != nullptr) {
					
					//push the parent as well as current child num onto stack
					p = std::make_pair(current, i);
					stack.push(p);
					
					current = children[i];
				//std::cout << current->getData() << std::endl;
				break;
				}
			}
			currentNum = 0;
		}
		
		else { //leaf therefore pop last state and move onto next sibling of parent.
			//check if we are on last node
			if (stack.empty()){
				//std::cout << "EMPTY" << std::endl;				
				//current = nullptr;
				end = true;
				iterator *toReturn = new iterator(); //creates a nullptr wrapped by iterator
				current = nullptr;
				return *toReturn;
			//	return *this;
			}
			
			p = stack.top();
			stack.pop();
			current = p.first;
			currentNum = p.second-1;
			operator --();
		}
	
	
		
		//std::cout << "ITERATOR is now on : " << current->getData() << std::endl;
		//iterator *toReturn = new iterator(current);
		return *this;
	
	}
	
	// -
	// moves iterator backwards n steps
	iterator& iterator::operator-(int n) {
		for (int i=0 ; i<n; ++i)
			operator--();
	}
	
	//dist between 2 iterators
	int iterator::operator-(iterator prev) {
		std::size_t n = 0;
		//std::cout << "OPERATOR -" << std::endl;
		while (!current->equals(*prev.getn())) {
			++prev;
			++n;
		}	
		return n;
	}
	 
 }
