/*
 * iterator.h
 * 
 * Created on 26 March 2014
 * Craig Feldman
 */

#ifndef PARSETREE_H
#define PARSETREE_H

//#include "iterator.h"
#include "node.h"
#include "iterator.h"
 #include <stdexcept>
 #include <random>				// For random number generation
 #include <chrono>				// For accessing the system clock
 #include <sstream>

namespace fldcra001 {
	class parsetree {
		
	private:
		node * root = nullptr;
		int counter = 0; 		//for testing

	public:

		//**************************************************
		
		//constructor
		parsetree() : root(nullptr) {} ;
		
		//destructor
		~parsetree();
		
		//copy constructor
		parsetree(const parsetree & rhs);
		
		//copy assignment
		parsetree & operator=(const parsetree & rhs);
		
		//move constructor
		parsetree(parsetree && rhs);
		
		//move assignment
		parsetree & operator=(parsetree && rhs);
		
		//************************************************
		
		//insert
		iterator insert(iterator parent,  node & child);
		
		//begin
		iterator begin(void) const;
		
		//end
		iterator end(void) const;
		
		//clear
		void clear(void);
		
		//size
		std::size_t size(void);
		
		//erase
		void erase(iterator & i);		
		
		//<<
		friend std::ostream& operator<<(std::ostream& os, const parsetree &tree);
		
		void allocate(int currentDepth, int totalDepth, iterator it);
		
		int getCounter(void) {return counter;}
		
		// equal
		bool equals(parsetree & other);
		
	}; //class
}//namespace

#endif
